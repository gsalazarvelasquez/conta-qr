export interface Customer {
  key?: string,
  fullName: string,
  nit: string
}
