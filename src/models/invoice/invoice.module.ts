export interface Invoice {
  key?: string,
  date: Date,
  nit: string,
  businessName: string,
  invoiceNumber: number,
  policyNumber: number,
  authNumber: string,
  totalBill: number,
  exempt: number,
  subTotalBill: number,
  desc: number,
  baseImp: number,
  cf: number,
  controlCode: string,
  compType: number

}
