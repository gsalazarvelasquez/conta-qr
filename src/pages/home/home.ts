import {Component} from '@angular/core';
import {IonicPage, Modal, ModalController, NavController} from 'ionic-angular';
import {BarcodeScanner} from "@ionic-native/barcode-scanner";
import {Customer} from "../../models/customer/customer.module";

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  scannedCode: String = null;
  qrCodeData: Array<String> = [];
  customers: Array<Customer> = [];


  constructor(public navCtrl: NavController, private modal: ModalController, private barcodeScanner: BarcodeScanner) {
    this.scannedCode = "122857024|13645|382401700054795|09/05/2018|215.00|215.00|33-13-83-96-0F|7918446|0|0|0|0";
    this.qrCodeData = this.scannedCode.split('|');
    console.log('qrCodeData: ', this.qrCodeData);
  }

  goToCustomerMonths(customer) {
    console.log('goToCustomerMonths', customer)
  }

  scanCode() {
    this.barcodeScanner.scan().then((data) => {
      this.scannedCode = data.text;
      this.qrCodeData = this.scannedCode.split('|');
      console.log('qrCodeData: ', this.qrCodeData);
    }, (err) => {
      console.log('Error: ', err);
    })
  }

  addCustomerModal() {
    const myModal: Modal = this.modal.create('ModalPage');
    myModal.present();

    myModal.onDidDismiss((customer: Customer) => {
      console.log('customer:', customer);
      if (customer != undefined)
        this.customers.push(customer);
    })
  }
}
