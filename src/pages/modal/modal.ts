import {Component} from '@angular/core';
import {IonicPage, NavParams, ViewController} from 'ionic-angular';
import {Customer} from "../../models/customer/customer.module";

@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {
  customer: Customer = {fullName: '', nit: undefined};
  isDataValid: boolean = false;

  constructor(private navParams: NavParams, private view: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalPage');
  }

  cancel() {
    this.view.dismiss();
  }

  save() {
    console.log('save: ', this.customer);
    this.view.dismiss(this.customer);
  }

  validateData() {
    this.isDataValid = this.customer.fullName != null &&
      this.customer.nit != null;
  }
}
